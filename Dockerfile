FROM python:3.6
WORKDIR /app
COPY handler.py requirements.txt tests ./
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python","./main.py"]