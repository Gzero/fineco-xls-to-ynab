# Build Docker Container

    docker build -t fineco-xls-to-ynab .

# Run Local docker

    # Be careful where you execute this code from, the secrets will be visible on terminal history.
    docker run -it --rm --entrypoint bash --env AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY --env AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY fineco-xls-to-ynab
    python3 s3_upload_file.py

# Deployment

## Execute SLS Docker (Optional)

Excute the serverless framework in docker version (only if you don't have sls already installed locally)

    docker run -it --rm --entrypoint bash \
    -v $(pwd):/opt/app \
    --env AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY \
    --env AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY \
    amaysim/serverless:3.19.0 

In order to install the package.json dependency:

    npm install
    or
    serverless plugin install -n serverless-python-requirements
     
## Deploy via SLS

    # Syntax
    sls deploy --stage <stage_name> --region <region>

    # Example
    sls deploy --stage dev --region eu-central-1