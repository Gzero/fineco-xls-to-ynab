import csv
import logging
import os
from tempfile import NamedTemporaryFile

import boto3 as boto3
from botocore.exceptions import ClientError
from openpyxl import load_workbook

HEADER_ROWS_NUMBER = 6

# YNAB CSV File Import requires the following columns:
#  Date | Payee | Memo | Outflow | Inflow
#
# Link: Link: https://docs.youneedabudget.com/article/921-formatting-csv-file
#
# In this python script we are also managing an additional field "Status", which tells if the transaction has been AUTHORIZED (UNCLEARED) or CLEARED.
# Since between the AUTHORIZATION and CLEARED Status may elapse a few days (usually 2-3) we need to be careful how we are importing these transaction to avoid duplicates.
#
# For Example:
#
# You get a coffe on date 01.01 - the transaction get added to bank transaction as "01.01 - Coffee - Authorized - EUR 2.00" , we import this transaction also to YNAB.
# After 2 days, the transaction now in the bank appear as:
# "03.01 - Coffee - Cleared - EUR 2.00", if this get imported to YNAB we are going to end up with 2 transaction:
# First: "01.01 - Coffee - Authorized - EUR 2.00"
# Second: "03.01 - Coffee - Cleared - EUR 2.00"
#
# True -> The CSV Generated will be fully compatible with YNAB File Importer
# False -> The CSV Will contain additional header columns which may not be compatible directly with YNAB File Importer
COMPATIBLE_WITH_YNAB_FILE_IMPORT = False

logging.getLogger().setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO, format='[%(levelname)s] %(message)s')
s3 = boto3.client('s3')


def download_file(bucket, key):
    try:
        file = NamedTemporaryFile(suffix='.xlsx', delete=False)
        logging.info(f"Downloading file from: {bucket}/{key}")
        s3.download_file(bucket, key, file.name)
        return file.name
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            return None
        else:
            raise
    else:
        raise


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(file_name)

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        logging.info(f"Uploading file: {bucket}/{object_name}")
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def convert(event, context):
    logging.info(f"Incoming Event: {event}")
    s3_bucket_name = event["Records"][0]['s3']['bucket']['name']
    xls_file_path = event["Records"][0]['s3']['object']['key']
    s3_object = download_file(s3_bucket_name, xls_file_path)

    logging.info(f"[Detected] Triggering S3 object Key: {xls_file_path}")

    filename, extension = os.path.basename(xls_file_path).split('.')
    logging.info(f"[Detected] Triggering File name: {filename}")
    logging.info(f"[Detected] Triggering File Extension: {extension}")

    wb = load_workbook(filename=s3_object)
    ws = wb.active
    max_rows = ws.max_row - HEADER_ROWS_NUMBER + 1

    # Spostiamo colonna "Uscite" al posto di "MoneyMap"
    ws.move_range(f"C1:C{ws.max_row}", rows=0, cols=4)
    logging.info("[Process] Sorting columns in correct order")
    # Spostiamo colonna "Entrate" alla destra di "MoneyMap" (stiamo aggiungendo una nuova colonna alla tabella)
    ws.move_range(f"B1:B{ws.max_row}", rows=0, cols=6)
    # Sposto Colonna "Descrizione Completa"
    ws.move_range(f"E1:E{ws.max_row}", rows=0, cols=-3)
    # Rimuoviamo colonna ora vuota dopo lo shift di "Descrizione completa"
    ws.delete_cols(4)
    # Rimuoviamo colonna "Descrizione"
    logging.info("[Process] Deleting Description column")
    ws.delete_cols(3)
    # Rimuoviamo le intestazioni relative al conto corrente
    logging.info("[Process] Deleting unused/useless columns")
    # Produce YNAB Importer Compatible File.
    if COMPATIBLE_WITH_YNAB_FILE_IMPORT:
        logging.info("[Process] Delete 'Status' Column")
        ws.delete_cols(4)
    ws.delete_rows(0, HEADER_ROWS_NUMBER)

    # Rinominiamo le colonne
    # Date | Payee | Memo | Outflow | Inflow
    ws['A1'] = "Date"
    ws['B1'] = "Payee"
    ws['C1'] = "Memo"
    if COMPATIBLE_WITH_YNAB_FILE_IMPORT:
        ws['D1'] = "Outflow"
        ws['E1'] = "Inflow"
    else:
        ws['D1'] = "Status"
        ws['E1'] = "Outflow"
        ws['F1'] = "Inflow"

    # If we are producing a COMPATIBLE_WITH_YNAB_FILE_IMPORT, we are going to have 1 less column
    COLUMN_INDEX_OUTFLOW = COMPATIBLE_WITH_YNAB_FILE_IMPORT and 4 or 5
    COLUMN_INDEX_STATUS = COMPATIBLE_WITH_YNAB_FILE_IMPORT and None or 4

    # Rendiamo i valori della colonna "Outlow" tutti positivi (Di default le uscite sono con il segno - davanti)
    for row in range(2, max_rows):
        cell = ws.cell(column=COLUMN_INDEX_OUTFLOW, row=row)
        if cell.value:
            cell.value = cell.value * -1
    logging.info("[Process] Mark Outflow as negative")

    # In the Italian Language we have 2 possible values for 'Status'
    # "Contabilizzato" = CLEARED
    # "Autorizzato" = UNCLEARED
    if not COMPATIBLE_WITH_YNAB_FILE_IMPORT:
        for row in range(2, max_rows):
            cell = ws.cell(column=COLUMN_INDEX_STATUS, row=row)
            if cell.value:
                if cell.value == "Contabilizzato":
                    cell.value = "CLEARED"
                elif cell.value == "Autorizzato":
                    cell.value = "NOT_CLEARED"
                else:
                    logging.error(f"Invalid value for 'Status' Column: {cell.value}")
                    raise Exception(f"Invalid value for 'Status' Column: {cell.value}")
        logging.info("[Process] Harmonized 'Status' Column")

    # Puliamo le descrizioni
    for row in range(2, max_rows):
        cell = ws.cell(column=2, row=row)
        if cell.value:
            if '     ' in str(cell.value):
                split = str(cell.value).split(sep='     ', maxsplit=1)
                if len(split) > 1:
                    cell.value = split[0]
    logging.info("[Process] Cleaning Up transactions descriptions")

    logging.info("Saving normalized file")
    wb.save(f"/tmp/{filename}_normalized.{extension}")

    # write csv
    logging.info("Saving CSV file for YNAB Import")
    with open(f"/tmp/{filename}.csv", 'w', newline="") as f:
        c = csv.writer(f, delimiter=';')
        for r in ws.rows:
            c.writerow([cell.value for cell in r])

    upload_file(file_name=f"/tmp/{filename}.csv",bucket=s3_bucket_name, object_name=f"output/{filename}.csv")
    logging.info(f"File Uploaded to: {s3_bucket_name}/output/{filename}.csv")